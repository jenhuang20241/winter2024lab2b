/*
* Assignment 3 2023 - 110
*
* runs a Wordle game
* automatically generates a target word
* asks user to guess words
* game keeps going until answered is revealed
* or game over with 6 attempts
*
* author: Jennifer Huang
* ID: 1437051
*/

import java.util.Scanner;
import java.util.Random;


public class Wordle {
	
	public static void startGame(){
		
		String answer = generateWord();
		runGame(answer);
		
	}
	
	
	
	/* initialize a new array filled with Wordle words
	 * randomly picks a word from the array
	 * returns the Wordle answer
	*/
	
	public static String generateWord(){
		
		String [] wordList = new String []
			{"DREAM", "ADULT", "SIGHT", "FORCE", "WOUND", "BRAVE", "CABLE", "STUDY", "PANIC", "GRADE", "DRIVE", "ALIEN", "PAUSE", "PARTY", "SHAME", "MATCH", "STORE", "RHYME", "PLANT", "BRAIN"};
			
		Random randGen = new Random();
		int idx = randGen.nextInt(wordList.length);
		String answer = wordList[idx];
		
		return answer;
	}
	
	
	
	/* compares the letter to each slot in answer
	 * returns true as soon as it finds a match
	*/
	
	public static boolean letterInWord(String answer, char letter){
	
		for (int i=0; i<answer.length(); i++){
			
			if (answer.charAt(i)==letter){
				return true;
			}
		}
		return false;
	}
	
	
	
	/* takes in letter, compares it to answer
	 * checks if letter is in THAT specific slot of answer
	*/
	
	public static boolean letterInSlot(String answer, char letter, int position){
	
		if (position<answer.length()){
			if (answer.charAt(position)==letter){
			return true;
			}
		}
		return false;
	}
	
	
	
	/* compares letter of guess to each slot of answer
	 * checks if letter is in target word
	 * if letter is not in the word, it is marked "white"
	 * if letter is in the word but not in right spot, it's "yellow"
	 * if letter is in the right position, it's "green"
	 * returns a String array with corresponding colours
	*/
	
	public static String [] guessWord(String answer, String guess){
		
		String [] colours = new String [answer.length()];
		
		
		for (int i=0; i<guess.length(); i++){
			
			if (letterInWord(answer, guess.charAt(i)) == true){
				
				if (letterInSlot(answer, guess.charAt(i), i) == true){
					colours[i] = "green";
				
				} else {
					colours[i] = "yellow";
				}
				
			} else {
				colours[i] = "white";
			}
		}
		return colours;
	}
	
	
	
	/* takes the colour array from guessWord
	 * applies the actual colour to the 5-letter guessed word
	*/
	
	public static void presentResults(String guess, String [] colours){
		
		
		for (int i=0; i<guess.length(); i++){
			
			if (colours[i] == "white"){
				
				System.out.print("\u001B[0m" + guess.charAt(i));
			
			} else if (colours[i] == "yellow"){
				
				System.out.print("\u001B[33m" + guess.charAt(i));
			
			} else if (colours[i] == "green"){
				
				System.out.print("\u001B[32m" + guess.charAt(i));
			}
		}
		System.out.println("\u001B[0m");
	}
	
	
	
	/* asks user to guess a word
	 * checks if it's 5-letters. If not, will ask again
	 * if valid, convert all to upper case and return
	*/
	
	public static String readGuess(){
	java.util.Scanner reader = new java.util.Scanner(System.in);
	
		System.out.println("Guess a word:");
		String guessInput = reader.nextLine();
		
		while (guessInput.length()!=5){
			System.out.println("Please enter a 5-letter word");
			guessInput = reader.nextLine();
		}
		
		
		String guessUpperCase = "";
		
		for (int i=0; i<guessInput.length(); i++){
			
			char guessLetter = guessInput.charAt(i);
			
			if (guessLetter >= 'a' && guessLetter <= 'z'){
				guessLetter -= 32;
				guessUpperCase += guessLetter;
			
			}  else {
				guessUpperCase += guessLetter;
			}
		}
		return guessUpperCase;
	}
	
	
	
	/* take in target word (answer)
	 * run readGuess to ask user to input word
	 * run guessWord & presentResults to display results
	 * do this in loop, 6 attempts allowed
	 * if they win, display "congrats!"
	 * if they ran out of attempts, displays "try again"
	*/
	
	public static void runGame(String answer){
		
		int attempts = 6;
		
		String guess = readGuess();
		presentResults(guess, guessWord(answer, guess));
		attempts--;
		System.out.println("Attempts Left: "+ attempts); 
		
		
		while (guess.equals(answer) == false && attempts>0) {
			
			guess = readGuess();
			presentResults(guess, guessWord(answer, guess));
			attempts--;
			System.out.println("Attempts Left: "+ attempts); 
			
		}
		
		if (guess.equals(answer) == true){ 
			System.out.println("Congratulations! You win!");
		}
		
		else if (guess.equals(answer) == false && attempts == 0) {
			System.out.println("The answer is: " + answer);
			System.out.println("You lose :/ try again");
		}
	}
}