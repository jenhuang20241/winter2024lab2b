import java.util.Scanner;

/*
* greets the user and asks which game they want to play
* player chooses game by inputting a number
* runs corresponding game
*/


public class GameLauncher {
	
	public static void main (String[] args){
	java.util.Scanner reader = new java.util.Scanner(System.in);
	
	
		System.out.println("Hi! What's your name?");
		String player = reader.nextLine();
		
		System.out.println("Welcome, "+ player +"! Choose a game: ");
		System.out.println("[1] Hangman");
		System.out.println("[2] Wordle");
		int gameSelect = reader.nextInt();
		
		
		//if neither game is chosen
		while (gameSelect!=1 && gameSelect!=2){
			
			System.out.println(
				"**Invalid input, please select from the following**");
			System.out.println("[1] Hangman");
			System.out.println("[2] Wordle");
			gameSelect = reader.nextInt();
			
		}
		
		//if Hangman is chosen
		if (gameSelect == 1){
			
			System.out.println("--Now playing Hangman--");
			Hangman.startGame();
		} 
		
		//if Wordle is chosen
		else if (gameSelect == 2) {
			
			System.out.println("--Now playing Wordle--");
			Wordle.startGame();
		}
		
	}

}